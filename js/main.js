const openCartButton = document.getElementById('open-cart');
const modalCart = document.getElementById('modal-cart')
const closeModalButton = document.getElementsByClassName("close")[0];

openCartButton.addEventListener('click', openCart);
function openCart(){
  modalCart.style.display = "block";
}

closeModalButton.addEventListener('click', closeModalBtn);
function closeModalBtn(){
  modalCart.style.display = "none";
}

window.addEventListener('click', closeModal);
function closeModal(event) {
    if (event.target == modalCart) {
        modalCart.style.display = "none";
    }
}

let removeCartItemButtons = document.getElementsByClassName("btn-danger");
for (let i = 0; i<removeCartItemButtons.length; i++){
  let button = removeCartItemButtons[i];
  button.addEventListener("click", removeCartItem)
}

let quantityInputs = document.getElementsByClassName('cart-quantity-input')
for (let i = 0; i<quantityInputs.length; i++){
  let input = quantityInputs[i]
  input.addEventListener('change', quantityChanged)
}

let addToCartButtons = document.getElementsByClassName('to-basket');
for (let i = 0; i<addToCartButtons.length; i++){
  let button = addToCartButtons[i]
  button.addEventListener('click', addToCartClicked)
}

document.getElementsByClassName('btn-remove-all')[0].addEventListener('click', removeAllClicked)
function removeAllClicked(){
  let cartItems = document.getElementsByClassName('cart-items')[0]
  while (cartItems.hasChildNodes()) {
    cartItems.removeChild(cartItems.firstChild)
  }
  updateCartTotal()
}

function removeCartItem(event){
  let buttonClicked = event.target;
  buttonClicked.parentElement.parentElement.remove()
  updateCartTotal()
}

function quantityChanged(event){
  let input = event.target
  if(isNaN(input.value) || input.value <= 0){
    input.value = 1
  }
  updateCartTotal()
}

function addToCartClicked(event){
  let button = event.target
  let shopItem = button.parentElement.parentElement
  let title = shopItem.getElementsByClassName('article-name')[0].innerText
  let price = shopItem.getElementsByClassName('new-price')[0].innerText
  let imageSrc = shopItem.getElementsByClassName('shop-item-image')[0].src
  console.log(title, price, imageSrc)
  addItemToCart(title, price, imageSrc)
  updateCartTotal();
}

function addItemToCart(title, price, imageSrc){
  let cartRow = document.createElement('div')
  cartRow.classList.add('cart-row')
  let cartItems = document.getElementsByClassName('cart-items')[0]
  let cartItemNames = cartItems.getElementsByClassName('cart-item-title')
  for (let i = 0; i < cartItemNames.length; i++){
    if(cartItemNames[i].innerText == title){
      let cartQuantityInput = document.getElementsByClassName('cart-quantity-input')[i];
        cartQuantityInput.value = parseInt(cartQuantityInput.value) + 1;
      updateCartTotal();
      return
    }
  }
  let cartRowContents = `
  <div class="cart-item cart-column">
      <img class="cart-item-image" src="${imageSrc}" width="100" height="100">
      <span class="cart-item-title">${title}</span>
  </div>
  <span class="cart-price cart-column">${price}</span>
  <div class="cart-quantity cart-column">
      <input class="cart-quantity-input" type="number" value="1">
      <button class="btn btn-danger" type="button">REMOVE</button>
  </div>`
  cartRow.innerHTML = cartRowContents
  cartItems.append(cartRow)
  cartRow.getElementsByClassName('btn-danger')[0].addEventListener('click', removeCartItem)
  cartRow.getElementsByClassName('cart-quantity-input')[0].addEventListener('change', quantityChanged)
}

function updateCartTotal(){
  let cartItemContauner = document.getElementsByClassName("cart-items")[0]
  let cartRows = cartItemContauner.getElementsByClassName('cart-row')
  let total = 0
  let counterTotal = 0
  let totalDiscount = 0
  for (let i = 0; i < cartRows.length; i++){
    let cartRow = cartRows[i]
    let priceElement = cartRow.getElementsByClassName('cart-price')[0]
    let quantityElement = cartRow.getElementsByClassName('cart-quantity-input')[0]

    counterTotal = parseInt(counterTotal) + parseInt(quantityElement.value)

    let price = parseFloat(priceElement.innerText.replace(' UAH', ''))
    let quantity = quantityElement.value
    total = total + (price * quantity);
    if (counterTotal > 5){
      totalDiscount = total;
      totalDiscount = totalDiscount - ((totalDiscount * 5)/100);
    } else {
      totalDiscount = total;
    }
  }
  total = Math.round(total * 100) / 100
  document.getElementsByClassName('cart-total-price')[0].innerText = total + " UAH"
  document.getElementsByClassName('cart-total-discount-price')[0].innerText = totalDiscount + " UAH"
  document.getElementById('counter').innerText = counterTotal
}
